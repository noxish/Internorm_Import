﻿Imports System.Configuration
Imports System.Reflection
Imports System.Collections.Specialized
Imports Microsoft.Win32
Imports System.Security.Cryptography
Imports Microsoft.VisualBasic.Logging
Imports System
Imports Microsoft.VisualBasic
Imports Microsoft.SqlServer.Management.Smo
Imports Microsoft.SqlServer.Management
Imports System.Diagnostics
Imports System.Management
Imports WindowsApp1.GetDatabase
Imports System.Data.Sql
Imports System.Data.SqlClient
Imports System.Data
Imports System.IO

Public Class Settings
    Public Sub New()
        ' Dieser Aufruf ist für den Designer erforderlich.
        InitializeComponent()
        If Registry.GetValue("HKEY_CURRENT_USER\Software\gs metalgroup\Settings", "Password", Nothing) Is Nothing Then
            Me.Password.Text = ""
        Else
            Dim Passwordd As String = Registry.GetValue("HKEY_CURRENT_USER\Software\gs metalgroup\Settings", "Password", Nothing)
            Me.Password.Text = System.Text.Encoding.UTF8.GetString(Convert.FromBase64String(Passwordd))
        End If

        If Registry.GetValue("HKEY_CURRENT_USER\Software\gs metalgroup\Settings", "Username", Nothing) Is Nothing Then
            Me.Username.Text = ""
        Else
            Me.Username.Text = Registry.GetValue("HKEY_CURRENT_USER\Software\gs metalgroup\Settings", "Username", Nothing)
        End If

        If Registry.GetValue("HKEY_CURRENT_USER\Software\gs metalgroup\Settings", "Database", Nothing) Is Nothing Then
            Me.MetroComboBox2.PromptText = ""
        Else
            Me.MetroComboBox2.PromptText = Registry.GetValue("HKEY_CURRENT_USER\Software\gs metalgroup\Settings", "Database", Nothing)
        End If

        If Registry.GetValue("HKEY_CURRENT_USER\Software\gs metalgroup\Settings", "Server", Nothing) Is Nothing Then
            Me.MetroComboBox1.PromptText = ""
        Else
            Me.MetroComboBox1.PromptText = Registry.GetValue("HKEY_CURRENT_USER\Software\gs metalgroup\Settings", "Server", Nothing)
        End If




    End Sub
    Private Sub MetroButton1_Click(sender As Object, e As EventArgs) Handles MetroButton1.Click
        Dim Speichern As Integer
        Speichern = MsgBox("Möchtest du die Daten wirklich Speichern?", vbYesNo + vbQuestion, "Wirklich Speichern?")

        If Speichern = vbYes Then
            Dim Passwordd
            Passwordd = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(Password.Text))

            Registry.SetValue("HKEY_CURRENT_USER\Software\gs metalgroup\Settings", "Username", Username.Text)
            Registry.SetValue("HKEY_CURRENT_USER\Software\gs metalgroup\Settings", "Password", Passwordd.ToString)

            'If String.IsNullOrEmpty(MetroComboBox1.SelectedText) Then
            '    Registry.SetValue("HKEY_CURRENT_USER\Software\gs metalgroup\Settings", "Server", MetroComboBox1.Text)
            'Else
            '    Registry.SetValue("HKEY_CURRENT_USER\Software\gs metalgroup\Settings", "Server", MetroComboBox1.SelectedText.ToString)
            'End If

            'If String.IsNullOrEmpty(MetroComboBox2.SelectedText) Then
            '    Registry.SetValue("HKEY_CURRENT_USER\Software\gs metalgroup\Settings", "Database", MetroComboBox2.PromptText)
            'Else
            '    Registry.SetValue("HKEY_CURRENT_USER\Software\gs metalgroup\Settings", "Database", MetroComboBox2.SelectedText.ToString)
            'End If

            Me.Close()
        Else
            'do nothing
        End If
    End Sub

    Private Sub MetroButton2_Click(sender As Object, e As EventArgs) Handles MetroButton2.Click
        Me.Close()
    End Sub

    Private Sub MetroComboBox1_MouseClick(sender As Object, e As MouseEventArgs) Handles MetroComboBox1.MouseClick
        If MetroComboBox1.Items.Count = 0 Then
            Dim x As DataTable = SmoApplication.EnumAvailableSqlServers(localOnly:=True)
            If x.Rows.Count > 0 Then
                For Each dr As DataRow In x.Rows
                    MetroComboBox1.Items.Add(dr("name").ToString())
                Next
            End If
        End If
    End Sub

    Private Sub MetroComboBox2_MouseClick(sender As Object, e As MouseEventArgs) Handles MetroComboBox2.MouseClick
        Call ListDatabases(MetroComboBox2)
    End Sub

    Private Sub MetroComboBox1_SelectedValueChanged(sender As Object, e As EventArgs) Handles MetroComboBox1.SelectedValueChanged
        Registry.SetValue("HKEY_CURRENT_USER\Software\gs metalgroup\Settings", "Server", MetroComboBox1.Text)
    End Sub
    Private Sub MetroComboBox2_SelectedValueChanged(sender As Object, e As EventArgs) Handles MetroComboBox2.SelectedValueChanged
        Registry.SetValue("HKEY_CURRENT_USER\Software\gs metalgroup\Settings", "Database", MetroComboBox2.Text)
    End Sub
End Class