﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Main
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.MetroButton1 = New MetroFramework.Controls.MetroButton()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.txt_description = New MetroFramework.Controls.MetroTextBox()
        Me.txt_breite = New MetroFramework.Controls.MetroTextBox()
        Me.txt_höhe = New MetroFramework.Controls.MetroTextBox()
        Me.txt_langtext = New MetroFramework.Controls.MetroTextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txt_fläche = New MetroFramework.Controls.MetroTextBox()
        Me.txt_article = New MetroFramework.Controls.MetroTextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txt_stück = New MetroFramework.Controls.MetroTextBox()
        Me.MetroComboBox1 = New MetroFramework.Controls.MetroComboBox()
        Me.MetroToggle1 = New MetroFramework.Controls.MetroToggle()
        Me.MetroButton2 = New MetroFramework.Controls.MetroButton()
        Me.MetroComboBox2 = New MetroFramework.Controls.MetroComboBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.MetroButton3 = New MetroFramework.Controls.MetroButton()
        Me.ComboPos = New MetroFramework.Controls.MetroComboBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txt_preis = New MetroFramework.Controls.MetroTextBox()
        Me.SuspendLayout()
        '
        'MetroButton1
        '
        Me.MetroButton1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MetroButton1.Location = New System.Drawing.Point(360, 412)
        Me.MetroButton1.Name = "MetroButton1"
        Me.MetroButton1.Size = New System.Drawing.Size(136, 56)
        Me.MetroButton1.TabIndex = 0
        Me.MetroButton1.Text = "Load File"
        Me.MetroButton1.UseSelectable = True
        '
        'txt_description
        '
        '
        '
        '
        Me.txt_description.CustomButton.Image = Nothing
        Me.txt_description.CustomButton.Location = New System.Drawing.Point(217, 1)
        Me.txt_description.CustomButton.Name = ""
        Me.txt_description.CustomButton.Size = New System.Drawing.Size(27, 27)
        Me.txt_description.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.txt_description.CustomButton.TabIndex = 1
        Me.txt_description.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.txt_description.CustomButton.UseSelectable = True
        Me.txt_description.CustomButton.Visible = False
        Me.txt_description.FontSize = MetroFramework.MetroTextBoxSize.Medium
        Me.txt_description.Lines = New String(-1) {}
        Me.txt_description.Location = New System.Drawing.Point(12, 131)
        Me.txt_description.MaxLength = 32767
        Me.txt_description.Name = "txt_description"
        Me.txt_description.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txt_description.ReadOnly = True
        Me.txt_description.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.txt_description.SelectedText = ""
        Me.txt_description.SelectionLength = 0
        Me.txt_description.SelectionStart = 0
        Me.txt_description.ShortcutsEnabled = True
        Me.txt_description.Size = New System.Drawing.Size(245, 29)
        Me.txt_description.TabIndex = 4
        Me.txt_description.UseSelectable = True
        Me.txt_description.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.txt_description.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'txt_breite
        '
        '
        '
        '
        Me.txt_breite.CustomButton.Image = Nothing
        Me.txt_breite.CustomButton.Location = New System.Drawing.Point(217, 1)
        Me.txt_breite.CustomButton.Name = ""
        Me.txt_breite.CustomButton.Size = New System.Drawing.Size(27, 27)
        Me.txt_breite.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.txt_breite.CustomButton.TabIndex = 1
        Me.txt_breite.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.txt_breite.CustomButton.UseSelectable = True
        Me.txt_breite.CustomButton.Visible = False
        Me.txt_breite.FontSize = MetroFramework.MetroTextBoxSize.Medium
        Me.txt_breite.Lines = New String(-1) {}
        Me.txt_breite.Location = New System.Drawing.Point(11, 180)
        Me.txt_breite.MaxLength = 32767
        Me.txt_breite.Name = "txt_breite"
        Me.txt_breite.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txt_breite.ReadOnly = True
        Me.txt_breite.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.txt_breite.SelectedText = ""
        Me.txt_breite.SelectionLength = 0
        Me.txt_breite.SelectionStart = 0
        Me.txt_breite.ShortcutsEnabled = True
        Me.txt_breite.Size = New System.Drawing.Size(245, 29)
        Me.txt_breite.TabIndex = 5
        Me.txt_breite.UseSelectable = True
        Me.txt_breite.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.txt_breite.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'txt_höhe
        '
        '
        '
        '
        Me.txt_höhe.CustomButton.Image = Nothing
        Me.txt_höhe.CustomButton.Location = New System.Drawing.Point(217, 1)
        Me.txt_höhe.CustomButton.Name = ""
        Me.txt_höhe.CustomButton.Size = New System.Drawing.Size(27, 27)
        Me.txt_höhe.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.txt_höhe.CustomButton.TabIndex = 1
        Me.txt_höhe.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.txt_höhe.CustomButton.UseSelectable = True
        Me.txt_höhe.CustomButton.Visible = False
        Me.txt_höhe.FontSize = MetroFramework.MetroTextBoxSize.Medium
        Me.txt_höhe.Lines = New String(-1) {}
        Me.txt_höhe.Location = New System.Drawing.Point(11, 229)
        Me.txt_höhe.MaxLength = 32767
        Me.txt_höhe.Name = "txt_höhe"
        Me.txt_höhe.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txt_höhe.ReadOnly = True
        Me.txt_höhe.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.txt_höhe.SelectedText = ""
        Me.txt_höhe.SelectionLength = 0
        Me.txt_höhe.SelectionStart = 0
        Me.txt_höhe.ShortcutsEnabled = True
        Me.txt_höhe.Size = New System.Drawing.Size(245, 29)
        Me.txt_höhe.TabIndex = 6
        Me.txt_höhe.UseSelectable = True
        Me.txt_höhe.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.txt_höhe.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'txt_langtext
        '
        Me.txt_langtext.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.txt_langtext.CustomButton.Image = Nothing
        Me.txt_langtext.CustomButton.Location = New System.Drawing.Point(4, 1)
        Me.txt_langtext.CustomButton.Name = ""
        Me.txt_langtext.CustomButton.Size = New System.Drawing.Size(369, 369)
        Me.txt_langtext.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.txt_langtext.CustomButton.TabIndex = 1
        Me.txt_langtext.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.txt_langtext.CustomButton.UseSelectable = True
        Me.txt_langtext.CustomButton.Visible = False
        Me.txt_langtext.Lines = New String(-1) {}
        Me.txt_langtext.Location = New System.Drawing.Point(264, 35)
        Me.txt_langtext.MaxLength = 32767
        Me.txt_langtext.Multiline = True
        Me.txt_langtext.Name = "txt_langtext"
        Me.txt_langtext.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txt_langtext.ReadOnly = True
        Me.txt_langtext.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.txt_langtext.SelectedText = ""
        Me.txt_langtext.SelectionLength = 0
        Me.txt_langtext.SelectionStart = 0
        Me.txt_langtext.ShortcutsEnabled = True
        Me.txt_langtext.Size = New System.Drawing.Size(374, 371)
        Me.txt_langtext.TabIndex = 7
        Me.txt_langtext.UseSelectable = True
        Me.txt_langtext.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.txt_langtext.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(10, 67)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(44, 13)
        Me.Label1.TabIndex = 8
        Me.Label1.Text = "Position"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(8, 115)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(72, 13)
        Me.Label2.TabIndex = 9
        Me.Label2.Text = "Beschreibung"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(8, 164)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(34, 13)
        Me.Label3.TabIndex = 10
        Me.Label3.Text = "Breite"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(8, 213)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(33, 13)
        Me.Label4.TabIndex = 11
        Me.Label4.Text = "Höhe"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(261, 19)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(48, 13)
        Me.Label5.TabIndex = 12
        Me.Label5.Text = "Langtext"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(8, 261)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(39, 13)
        Me.Label6.TabIndex = 14
        Me.Label6.Text = "Fläche"
        '
        'txt_fläche
        '
        '
        '
        '
        Me.txt_fläche.CustomButton.Image = Nothing
        Me.txt_fläche.CustomButton.Location = New System.Drawing.Point(217, 1)
        Me.txt_fläche.CustomButton.Name = ""
        Me.txt_fläche.CustomButton.Size = New System.Drawing.Size(27, 27)
        Me.txt_fläche.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.txt_fläche.CustomButton.TabIndex = 1
        Me.txt_fläche.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.txt_fläche.CustomButton.UseSelectable = True
        Me.txt_fläche.CustomButton.Visible = False
        Me.txt_fläche.FontSize = MetroFramework.MetroTextBoxSize.Medium
        Me.txt_fläche.Lines = New String(-1) {}
        Me.txt_fläche.Location = New System.Drawing.Point(11, 277)
        Me.txt_fläche.MaxLength = 32767
        Me.txt_fläche.Name = "txt_fläche"
        Me.txt_fläche.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txt_fläche.ReadOnly = True
        Me.txt_fläche.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.txt_fläche.SelectedText = ""
        Me.txt_fläche.SelectionLength = 0
        Me.txt_fläche.SelectionStart = 0
        Me.txt_fläche.ShortcutsEnabled = True
        Me.txt_fläche.Size = New System.Drawing.Size(245, 29)
        Me.txt_fläche.TabIndex = 13
        Me.txt_fläche.UseSelectable = True
        Me.txt_fläche.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.txt_fläche.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'txt_article
        '
        '
        '
        '
        Me.txt_article.CustomButton.Image = Nothing
        Me.txt_article.CustomButton.Location = New System.Drawing.Point(91, 1)
        Me.txt_article.CustomButton.Name = ""
        Me.txt_article.CustomButton.Size = New System.Drawing.Size(27, 27)
        Me.txt_article.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.txt_article.CustomButton.TabIndex = 1
        Me.txt_article.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.txt_article.CustomButton.UseSelectable = True
        Me.txt_article.CustomButton.Visible = False
        Me.txt_article.FontSize = MetroFramework.MetroTextBoxSize.Medium
        Me.txt_article.Lines = New String(-1) {}
        Me.txt_article.Location = New System.Drawing.Point(136, 83)
        Me.txt_article.MaxLength = 32767
        Me.txt_article.Name = "txt_article"
        Me.txt_article.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txt_article.ReadOnly = True
        Me.txt_article.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.txt_article.SelectedText = ""
        Me.txt_article.SelectionLength = 0
        Me.txt_article.SelectionStart = 0
        Me.txt_article.ShortcutsEnabled = True
        Me.txt_article.Size = New System.Drawing.Size(119, 29)
        Me.txt_article.TabIndex = 15
        Me.txt_article.UseSelectable = True
        Me.txt_article.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.txt_article.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(133, 67)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(82, 13)
        Me.Label7.TabIndex = 16
        Me.Label7.Text = "Referenz Artikel"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(131, 311)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(81, 13)
        Me.Label8.TabIndex = 20
        Me.Label8.Text = "Mengen Einheit"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(8, 311)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(35, 13)
        Me.Label9.TabIndex = 18
        Me.Label9.Text = "Stück"
        '
        'txt_stück
        '
        '
        '
        '
        Me.txt_stück.CustomButton.Image = Nothing
        Me.txt_stück.CustomButton.Location = New System.Drawing.Point(91, 1)
        Me.txt_stück.CustomButton.Name = ""
        Me.txt_stück.CustomButton.Size = New System.Drawing.Size(27, 27)
        Me.txt_stück.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.txt_stück.CustomButton.TabIndex = 1
        Me.txt_stück.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.txt_stück.CustomButton.UseSelectable = True
        Me.txt_stück.CustomButton.Visible = False
        Me.txt_stück.FontSize = MetroFramework.MetroTextBoxSize.Medium
        Me.txt_stück.Lines = New String(-1) {}
        Me.txt_stück.Location = New System.Drawing.Point(11, 327)
        Me.txt_stück.MaxLength = 32767
        Me.txt_stück.Name = "txt_stück"
        Me.txt_stück.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txt_stück.ReadOnly = True
        Me.txt_stück.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.txt_stück.SelectedText = ""
        Me.txt_stück.SelectionLength = 0
        Me.txt_stück.SelectionStart = 0
        Me.txt_stück.ShortcutsEnabled = True
        Me.txt_stück.Size = New System.Drawing.Size(119, 29)
        Me.txt_stück.TabIndex = 17
        Me.txt_stück.UseSelectable = True
        Me.txt_stück.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.txt_stück.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'MetroComboBox1
        '
        Me.MetroComboBox1.FormattingEnabled = True
        Me.MetroComboBox1.ItemHeight = 23
        Me.MetroComboBox1.Items.AddRange(New Object() {"Stück", "VE", "Meter", "kg", "tonne", "m²", "m³", "pauschal", "Std.", "Min.", "Tage", "km", "Einheit(en)"})
        Me.MetroComboBox1.Location = New System.Drawing.Point(134, 327)
        Me.MetroComboBox1.Name = "MetroComboBox1"
        Me.MetroComboBox1.Size = New System.Drawing.Size(121, 29)
        Me.MetroComboBox1.TabIndex = 21
        Me.MetroComboBox1.UseSelectable = True
        '
        'MetroToggle1
        '
        Me.MetroToggle1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MetroToggle1.AutoSize = True
        Me.MetroToggle1.Checked = True
        Me.MetroToggle1.CheckState = System.Windows.Forms.CheckState.Checked
        Me.MetroToggle1.Location = New System.Drawing.Point(558, 12)
        Me.MetroToggle1.Name = "MetroToggle1"
        Me.MetroToggle1.Size = New System.Drawing.Size(80, 17)
        Me.MetroToggle1.TabIndex = 22
        Me.MetroToggle1.Text = "An"
        Me.MetroToggle1.UseSelectable = True
        '
        'MetroButton2
        '
        Me.MetroButton2.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MetroButton2.Location = New System.Drawing.Point(502, 412)
        Me.MetroButton2.Name = "MetroButton2"
        Me.MetroButton2.Size = New System.Drawing.Size(136, 56)
        Me.MetroButton2.TabIndex = 23
        Me.MetroButton2.Text = "Position Importieren"
        Me.MetroButton2.UseSelectable = True
        '
        'MetroComboBox2
        '
        Me.MetroComboBox2.DropDownWidth = 300
        Me.MetroComboBox2.FormattingEnabled = True
        Me.MetroComboBox2.ItemHeight = 23
        Me.MetroComboBox2.Location = New System.Drawing.Point(13, 35)
        Me.MetroComboBox2.Name = "MetroComboBox2"
        Me.MetroComboBox2.Size = New System.Drawing.Size(242, 29)
        Me.MetroComboBox2.TabIndex = 24
        Me.MetroComboBox2.UseSelectable = True
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(10, 19)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(90, 13)
        Me.Label10.TabIndex = 25
        Me.Label10.Text = "E•R•Plus Projekt"
        '
        'MetroButton3
        '
        Me.MetroButton3.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.MetroButton3.Location = New System.Drawing.Point(13, 412)
        Me.MetroButton3.Name = "MetroButton3"
        Me.MetroButton3.Size = New System.Drawing.Size(136, 56)
        Me.MetroButton3.TabIndex = 26
        Me.MetroButton3.Text = "Einstellungen"
        Me.MetroButton3.UseSelectable = True
        '
        'ComboPos
        '
        Me.ComboPos.FormattingEnabled = True
        Me.ComboPos.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.ComboPos.ItemHeight = 23
        Me.ComboPos.Location = New System.Drawing.Point(13, 83)
        Me.ComboPos.Name = "ComboPos"
        Me.ComboPos.Size = New System.Drawing.Size(117, 29)
        Me.ComboPos.TabIndex = 28
        Me.ComboPos.UseSelectable = True
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(10, 361)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(30, 13)
        Me.Label11.TabIndex = 30
        Me.Label11.Text = "Preis"
        '
        'txt_preis
        '
        '
        '
        '
        Me.txt_preis.CustomButton.Image = Nothing
        Me.txt_preis.CustomButton.Location = New System.Drawing.Point(217, 1)
        Me.txt_preis.CustomButton.Name = ""
        Me.txt_preis.CustomButton.Size = New System.Drawing.Size(27, 27)
        Me.txt_preis.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.txt_preis.CustomButton.TabIndex = 1
        Me.txt_preis.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.txt_preis.CustomButton.UseSelectable = True
        Me.txt_preis.CustomButton.Visible = False
        Me.txt_preis.FontSize = MetroFramework.MetroTextBoxSize.Medium
        Me.txt_preis.Lines = New String(-1) {}
        Me.txt_preis.Location = New System.Drawing.Point(13, 377)
        Me.txt_preis.MaxLength = 32767
        Me.txt_preis.Name = "txt_preis"
        Me.txt_preis.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txt_preis.ReadOnly = True
        Me.txt_preis.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.txt_preis.SelectedText = ""
        Me.txt_preis.SelectionLength = 0
        Me.txt_preis.SelectionStart = 0
        Me.txt_preis.ShortcutsEnabled = True
        Me.txt_preis.Size = New System.Drawing.Size(245, 29)
        Me.txt_preis.TabIndex = 29
        Me.txt_preis.UseSelectable = True
        Me.txt_preis.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.txt_preis.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'Main
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(650, 480)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.txt_preis)
        Me.Controls.Add(Me.ComboPos)
        Me.Controls.Add(Me.MetroButton3)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.MetroComboBox2)
        Me.Controls.Add(Me.MetroButton2)
        Me.Controls.Add(Me.MetroToggle1)
        Me.Controls.Add(Me.MetroComboBox1)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.txt_stück)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.txt_article)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.txt_fläche)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txt_langtext)
        Me.Controls.Add(Me.txt_höhe)
        Me.Controls.Add(Me.txt_breite)
        Me.Controls.Add(Me.txt_description)
        Me.Controls.Add(Me.MetroButton1)
        Me.Name = "Main"
        Me.ShowIcon = False
        Me.Text = "Internorm > E•R•Plus"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents MetroButton1 As MetroFramework.Controls.MetroButton
    Friend WithEvents OpenFileDialog1 As OpenFileDialog
    Friend WithEvents txt_description As MetroFramework.Controls.MetroTextBox
    Friend WithEvents txt_breite As MetroFramework.Controls.MetroTextBox
    Friend WithEvents txt_höhe As MetroFramework.Controls.MetroTextBox
    Friend WithEvents txt_langtext As MetroFramework.Controls.MetroTextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents txt_fläche As MetroFramework.Controls.MetroTextBox
    Friend WithEvents txt_article As MetroFramework.Controls.MetroTextBox
    Friend WithEvents Label7 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents txt_stück As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroComboBox1 As MetroFramework.Controls.MetroComboBox
    Friend WithEvents MetroToggle1 As MetroFramework.Controls.MetroToggle
    Friend WithEvents MetroButton2 As MetroFramework.Controls.MetroButton
    Friend WithEvents MetroComboBox2 As MetroFramework.Controls.MetroComboBox
    Friend WithEvents Label10 As Label
    Friend WithEvents MetroButton3 As MetroFramework.Controls.MetroButton
    Friend WithEvents ComboPos As MetroFramework.Controls.MetroComboBox
    Friend WithEvents Label11 As Label
    Friend WithEvents txt_preis As MetroFramework.Controls.MetroTextBox
End Class
