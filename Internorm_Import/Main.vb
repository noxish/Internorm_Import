﻿Imports Microsoft.Win32
Imports System.Threading
Imports System.IO
Imports System.Data
Imports System.Xml
Imports System.Xml.Schema
Imports System.Xml.XPath
Imports System.Data.Sql
Imports System.Data.SqlClient
Imports Microsoft.SqlServer.Management.Smo
Imports System.Security.Cryptography
Imports System.Collections.Specialized
Imports WindowsApp1.XmlHelper

Public Class Main
    Dim dt As New DataTable
    Public Sub Main(OpenFile As String)
        Dim fileName As String = OpenFile
        Dim doc As New XmlDocument
        If String.IsNullOrEmpty(fileName) Then
            'do nothing
            Exit Sub
        Else
            doc.Load(fileName)
        End If


        dt.Columns.Add("Pos")
        dt.Columns.Add("PosBeschreibung")
        dt.Columns.Add("ReferenzArtikel")
        dt.Columns.Add("Langtext")
        dt.Columns.Add("Stück")
        dt.Columns.Add("Breite")
        dt.Columns.Add("Höhe")
        dt.Columns.Add("Fläche")
        dt.Columns.Add("Preis")

        Dim dr As DataRow = dt.NewRow

        Dim PosNr As XmlNodeList = doc.SelectNodes("/DocSys/SyncOffHdr/SyncOffPos/OffSNr")
        Dim Descrip As XmlNodeList = doc.SelectNodes("/DocSys/SyncOffHdr/SyncOffPos/WarDsc")
        Dim Ref As XmlNodeList = doc.SelectNodes("/DocSys/SyncOffHdr/SyncOffPos/ReferenzArtikel")
        Dim Langtext As XmlNodeList = doc.SelectNodes("/DocSys/SyncOffHdr/SyncOffPos/PrdTxt_Content")
        Dim Stück As XmlNodeList = doc.SelectNodes("/DocSys/SyncOffHdr/SyncOffPos/QtyBas")
        Dim Breite As XmlNodeList = doc.SelectNodes("/DocSys/SyncOffHdr/SyncOffPos/PrmPar01")
        Dim Höhe As XmlNodeList = doc.SelectNodes("/DocSys/SyncOffHdr/SyncOffPos/PrmPar02")
        Dim Preis As XmlNodeList = doc.SelectNodes("/DocSys/SyncOffHdr/SyncOffPos/FNtPrc")

        For i As Integer = 0 To PosNr.Count - 1
            Dim BreiteCon As String = Breite.Item(i).InnerText
            Dim HöheCon As String = Höhe.Item(i).InnerText
            Dim PreisCon As String = Preis.Item(i).InnerText
            Dim Breiteto As String = BreiteCon.Replace(".", "")
            Dim Höheto As String = HöheCon.Replace(".", "")
            Dim Preisto As String = PreisCon.Replace(",", ".")
            Dim BreiteInt As Int64
            Dim HöheInt As Int64
            If String.IsNullOrEmpty(Breiteto) = True Or String.IsNullOrEmpty(Höheto) = True Then
                BreiteInt = 0
                HöheInt = 0
            Else

                BreiteInt = Breiteto
                HöheInt = Höheto
            End If

            Dim FlächeKomma As String = (BreiteInt / 1000) * (HöheInt / 1000)
            Dim Fläche As String = FlächeKomma.Replace(",", ".")

            dt.Rows.Add(PosNr.Item(i).InnerText, Descrip.Item(i).InnerText, Ref.Item(i).InnerText, Langtext.Item(i).InnerText, Stück.Item(i).InnerText, BreiteInt, HöheInt, Fläche, Preisto)
        Next

        ComboPos.DataSource = dt
        ComboPos.DisplayMember = "Pos"
        MetroComboBox1.SelectedIndex = 0
    End Sub

    Private Sub ComboPos_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboPos.SelectedIndexChanged
        Dim tableindex As String = ComboPos.SelectedIndex
        txt_article.Text = dt.Rows(tableindex).Item("ReferenzArtikel")
        txt_description.Text = dt.Rows(tableindex).Item("PosBeschreibung")
        txt_langtext.Text = dt.Rows(tableindex).Item("Langtext")
        txt_breite.Text = dt.Rows(tableindex).Item("Breite")
        txt_höhe.Text = dt.Rows(tableindex).Item("Höhe")
        txt_fläche.Text = dt.Rows(tableindex).Item("Fläche")
        txt_stück.Text = dt.Rows(tableindex).Item("Stück")
        txt_preis.Text = dt.Rows(tableindex).Item("Preis")
    End Sub

    Private Sub MetroButton1_Click(sender As Object, e As EventArgs) Handles MetroButton1.Click
        OpenFileDialog1.ShowDialog()
        Call Main(OpenFileDialog1.FileName)
    End Sub

    Private Sub MetroToggle1_CheckedChanged(sender As Object, e As EventArgs) Handles MetroToggle1.CheckedChanged
        If MetroToggle1.CheckState = 1 Then
            txt_article.ReadOnly = 1
            txt_description.ReadOnly = 1
            txt_breite.ReadOnly = 1
            txt_höhe.ReadOnly = 1
            txt_fläche.ReadOnly = 1
            txt_langtext.ReadOnly = 1
            txt_stück.ReadOnly = 1
            MetroComboBox1.Enabled = 0
        Else
            txt_article.ReadOnly = 0
            txt_description.ReadOnly = 0
            txt_breite.ReadOnly = 0
            txt_höhe.ReadOnly = 0
            txt_fläche.ReadOnly = 0
            txt_langtext.ReadOnly = 0
            txt_stück.ReadOnly = 0
            MetroComboBox1.Enabled = 1
        End If
    End Sub

    Private Sub MetroButton3_Click(sender As Object, e As EventArgs) Handles MetroButton3.Click
        Dim frm As New Settings
        frm.Show()
    End Sub
    Dim myData As New DataTable
    Private Sub MetroComboBox2_MouseClick(sender As Object, e As MouseEventArgs) Handles MetroComboBox2.MouseClick
        Dim conn As SqlConnection
        Dim myCommand As New SqlCommand
        Dim myAdapter As New SqlDataAdapter
        Dim dataset As New DataSet
        Dim SQL As String


        Dim Password As String = Registry.GetValue("HKEY_CURRENT_USER\Software\gs metalgroup\Settings", "Password", Nothing)

        conn = New SqlConnection
        conn.ConnectionString = "server=" & Registry.GetValue("HKEY_CURRENT_USER\Software\gs metalgroup\Settings", "Server", Nothing) & ";" & "user id=" & Registry.GetValue("HKEY_CURRENT_USER\Software\gs metalgroup\Settings", "Username", Nothing) & ";" _
        & "password=" & System.Text.Encoding.UTF8.GetString(Convert.FromBase64String(Password)) & ";"
        Try
            conn.Open()
            SQL = "use [" & Registry.GetValue("HKEY_CURRENT_USER\Software\gs metalgroup\Settings", "Database", Nothing) & "]; select b.[Kommissions-Nr] As Kommi, b.[Kommissions-Nr] + ' ' + b.bauvorhaben As Projekt from bauvorhaben b where Abgeschlossen = 0 AND (SELECT COUNT(k.[Pos-Nr]) FROM kalkulationsstruktur k WHERE b.[Kommissions-Nr] = k.[Kommissions-Nr]) = 0"

            myCommand.Connection = conn
            myCommand.CommandText = SQL
            myData.Clear()
            myAdapter.SelectCommand = myCommand
            myAdapter.Fill(myData)

            MetroComboBox2.DataSource = myData
            MetroComboBox2.DisplayMember = "Projekt"

            conn.Close()
        Catch myerror As SqlException
            MessageBox.Show(myerror.Message)
        Finally

        End Try
    End Sub

    Private Sub MetroButton2_Click(sender As Object, e As EventArgs) Handles MetroButton2.Click

        Dim conn As SqlConnection
        Dim myCommand As New SqlCommand
        Dim myAdapter As New SqlDataAdapter
        Dim dataset As New DataSet
        Dim SQL As String
        Dim Password As String = Registry.GetValue("HKEY_CURRENT_USER\Software\gs metalgroup\Settings", "Password", Nothing)
        Dim Database As String = Registry.GetValue("HKEY_CURRENT_USER\Software\gs metalgroup\Settings", "Database", Nothing)
        conn = New SqlConnection
        conn.ConnectionString = "server=" & Registry.GetValue("HKEY_CURRENT_USER\Software\gs metalgroup\Settings", "Server", Nothing) & ";" & "user id=" & Registry.GetValue("HKEY_CURRENT_USER\Software\gs metalgroup\Settings", "Username", Nothing) & ";" _
    & "password=" & System.Text.Encoding.UTF8.GetString(Convert.FromBase64String(Password)) & ";"
        Dim projektindex As String = MetroComboBox2.SelectedIndex
        Dim result As Integer
        If String.IsNullOrEmpty(ComboPos.Text) = True Or String.IsNullOrEmpty(MetroComboBox2.Text) = True Then
            'do Nothing
        Else
            result = MessageBox.Show("Sollen Alle oder nur die Angezeigte Position Importiert werden?", "Frage", MessageBoxButtons.YesNoCancel)
        End If

        If result = DialogResult.Cancel Then
                Exit Sub
            ElseIf result = DialogResult.No Then
                Try
                    conn.Open()
                SQL = "use [" & Database & "]; INSERT Into [kalkulationsstruktur] ([Kommissions-Nr],[Position],[Anzahl],[Bezeichnung],[Beschreibung],[MengenEinheit],[Bild1],[Bild2],[Bild3],[Bild4],[Bild5],[Bedarfsposition],[Verteilt],[Nachtragsposition],[Breite],[Höhe],[Fläche],[AngDetID],[Beauftragt],[Ebene],[Parent],[OrderColumn],[NKrelevant],[Erstellt_durch],[Rechnungsposition],[NachlassProzent],[ABDetID],[Markierung],[Bild1_Text],[Positionstyp],[scItemID],[scJobID],[LastChanged],[IsGaebPos],[ORGA],[Pauschaliert],[AnzahlBeauftragt],[Warengruppe],[TextFlag],[IntrastatGewicht]) Values ( '" & myData.Rows(projektindex).Item("Kommi") & "','" & ComboPos.Text & "','" & txt_stück.Text & "','" & txt_description.Text & "','" & txt_langtext.Text & "','" & MetroComboBox1.Text & "',NULL,NULL,NULL,NULL,NULL,0,0,0," & txt_breite.Text & "," & txt_höhe.Text & "," & txt_fläche.Text & ",0,0,0,-1,1,-1,'Internorm Import',-1,0,0,0,NULL,0,NULL,NULL,NULL,0,NULL,0,0,NULL,0,0)" _
                    & "Declare @posnr varchar(max); " _
                    & "Select @posnr = (select [Pos-Nr] from kalkulationsstruktur where Position = '" & ComboPos.Text & "' And [Kommissions-Nr] = '" & myData.Rows(projektindex).Item("Kommi") & "') " _
                    & "insert into dbo.projektkosten ([Pos-Nr],Kostenart, Bezeichnung, [Group],Wert) " _
                    & "Select @posnr, Kurzname, Bezeichnung, Kostengruppe, '" & txt_preis.Text & "' from dbo.kostenstellen " _
                    & "where Kurzname = '***' " _
                    & "Declare @kst varchar(max) " _
                    & "Select @kst = (select pk.ID from projektkosten pk " _
                    & "inner Join kalkulationsstruktur kks ON kks.[Pos-Nr] = pk.[Pos-Nr] " _
                    & "where pk.[Pos-Nr] = @posnr) " _
                    & "Insert into projektartikelkalkulation (KstID, [Artikel-Nr], Artikelbezeichnung, Menge, MengenEinheit, Breite, Höhe, qm, Gesamtpreis, Listenpreis) Values (@kst,'0', '" & txt_description.Text & "','" & txt_stück.Text & "','" & MetroComboBox1.Text & "', " & txt_breite.Text & " , " & txt_höhe.Text & " , " & txt_fläche.Text & " ,'" & txt_preis.Text & "','" & txt_preis.Text & "') "
                myCommand.Connection = conn
                    myCommand.CommandText = SQL
                    myAdapter.SelectCommand = myCommand
                    myAdapter.Fill(myData)
                    conn.Close()
                Catch myerror As SqlException
                    MessageBox.Show(myerror.Message)
                Finally

                End Try
            ElseIf result = DialogResult.Yes Then
                Dim Row As DataRow
            For Each Row In dt.Rows
                Try
                    conn.Open()
                    SQL = "use [" & Database & "]; INSERT Into [kalkulationsstruktur] ([Kommissions-Nr],[Position],[Anzahl],[Bezeichnung],[Beschreibung],[MengenEinheit],[Bild1],[Bild2],[Bild3],[Bild4],[Bild5],[Bedarfsposition],[Verteilt],[Nachtragsposition],[Breite],[Höhe],[Fläche],[AngDetID],[Beauftragt],[Ebene],[Parent],[OrderColumn],[NKrelevant],[Erstellt_durch],[Rechnungsposition],[NachlassProzent],[ABDetID],[Markierung],[Bild1_Text],[Positionstyp],[scItemID],[scJobID],[LastChanged],[IsGaebPos],[ORGA],[Pauschaliert],[AnzahlBeauftragt],[Warengruppe],[TextFlag],[IntrastatGewicht]) Values ( '" & myData.Rows(projektindex).Item("Kommi") & "','" & Row.Item("Pos") & "','" & Row.Item("Stück") & "','" & Row.Item("PosBeschreibung") & "','" & Row.Item("Langtext") & "','" & MetroComboBox1.Text & "',NULL,NULL,NULL,NULL,NULL,0,0,0," & Row.Item("Breite") & "," & Row.Item("Höhe") & "," & Row.Item("Fläche") & ",0,0,0,-1,1,-1,'Internorm Import',-1,0,0,0,NULL,0,NULL,NULL,NULL,0,NULL,0,0,NULL,0,0)" _
                     & "Declare @posnr varchar(max); " _
                    & "Select @posnr = (select [Pos-Nr] from kalkulationsstruktur where Position = '" & Row.Item("Pos") & "' And [Kommissions-Nr] = '" & myData.Rows(projektindex).Item("Kommi") & "') " _
                    & "insert into dbo.projektkosten ([Pos-Nr],Kostenart, Bezeichnung, [Group],Wert) " _
                    & "Select @posnr, Kurzname, Bezeichnung, Kostengruppe, '" & Row.Item("Preis") & "' from dbo.kostenstellen " _
                    & "where Kurzname = '***' " _
                    & "Declare @kst varchar(max) " _
                    & "Select @kst = (select pk.ID from projektkosten pk " _
                    & "inner Join kalkulationsstruktur kks ON kks.[Pos-Nr] = pk.[Pos-Nr] " _
                    & "where pk.[Pos-Nr] = @posnr) " _
                    & "Insert into projektartikelkalkulation (KstID, [Artikel-Nr], Artikelbezeichnung, Menge, MengenEinheit, Breite, Höhe, qm, Gesamtpreis, Listenpreis) Values (@kst,'0', '" & Row.Item("PosBeschreibung") & "','" & Row.Item("Stück") & "','" & MetroComboBox1.Text & "', " & Row.Item("Breite") & " , " & Row.Item("Höhe") & " , " & Row.Item("Fläche") & " ,'" & Row.Item("Preis") & "','" & Row.Item("Preis") & "') "

                    myCommand.Connection = conn
                    myCommand.CommandText = SQL
                    myAdapter.SelectCommand = myCommand
                    myAdapter.Fill(myData)
                    conn.Close()
                Catch myerror As SqlException
                    MessageBox.Show(myerror.Message)
                Finally

                End Try
            Next
        End If
    End Sub

End Class
